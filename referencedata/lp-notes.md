# Enige opmerkingen aangaande de HisGis locatiepunten

De adressen in de HisGis Locatiepuntenset zijn niet met Adamlink straten verbonden, waardoor ten eerste koppelingen lastig te leggen zijn, en ten tweede de kwaliteit van de locatiepunten hier en daar te wensen overlaat.

Leon heeft dit naderhand wel gedaan, en ik heb zijn set als uitgangspunt genomen voor de adresboeken. Zo'n 290 in de locatiepunten voorkomende straten heeft hij niet kunnen matchen. Dit heb ik - deels - alsnog gedaan in [adamlink-matches-in-lp.csv](adamlink-matches-in-lp.csv).

Er zijn verschillende oorzaken waardoor het matchen niet gelukt lijkt, hieronder enkele voorbeelden:

### afwijkende spelling

- Réaumurstraat vs Reaumurstraat
- President Steijnstraat vs President Steynstraat

### gedeeltelijk van de kaart overgenomen straatnamen

- Hooftstraat ipv P.C. Hooftstraat
- Pieter Heijestraat ipv Jan Pieter Heijestraat
- Nassaulaan ipv Oranje-Nassaulaan
- Van Barneveldtkade ipv Van Oldenbarneveldtkade


### fout gelezen straatnamen

- Oelebesstraat voor Celebesstraat
- Jacob van Gennepstraat
- Oude Kerkstraat voor Oudekerksplein
- Vijgenlaan voor Vijgendam
- Baukenweg voor Beukenweg


### straten in gemeente Diemen zijn opgenomen

- Adressen aan de Johan Coussetstraat
- Adressen aan de Reinier Castelijnstraat
- Adressen aan de Johan van Soesdijkstraat

### indertijd in Watergraafsmeer gelegen adressen zijn opgenomen

- Steijnstraat (na annexatie 1921 Celsiusstraat)
- Dr. Leijdsstraat (na annexatie 1921 Torricellistraat)
- Bothastraat (na annexatie 1921 Réaumurstraat)

### andere aanduidingen op de kaart aangezien voor straatnamen

- Rijwielpad (een rijwielpad op de Oostelijke Handelskade)
- Kraanspoort (een kraanspoor in de haven bij de Stadsrietlanden)
- Singelgracht (naam van water, gebruikt voor enkele adressen aan de Alexanderkade)


### straten die nog niet in Adamlink voorkwamen

- Plantage Lijnbaansgracht
- Nieuwe Lijnbaansgracht
- Galgenpad
- Gallerij
- Spaarndammerplein

Deze straten zijn aangemaakt. Ook zijn er bestaande straten van geometrie of naamvarianten voorzien, of juist van verkeerd toegewezen naamvarianten ontdaan. Daarnaast zijn dateringen aangepast (van belang omdat het align-script uitging van straten die al voor 1910 bestonden, en daardoor sommige straten gemist werden).


