<?php

include("../settings.php");

$run = false;

$sql = "SELECT * FROM `observations`  
WHERE txt_lastname is null 
and part = 'profession'  
ORDER BY id ASC";

//$sql = "SELECT *  FROM `observations` WHERE `txt_street` LIKE '%' ORDER BY `txt_street`  DESC";

$result = $mysqli->query($sql);



while($row = $result->fetch_assoc()){ 

	$txt = $row['txt'];

	if(preg_match("/[0-9]+(\.|,|[a-z])?$/",$txt)){

		$s = "SELECT * FROM `observations`  
		WHERE id < " . $row['id'] . "  
		ORDER BY id DESC LIMIT 1";

		//$sql = "SELECT *  FROM `observations` WHERE `txt_street` LIKE '%' ORDER BY `txt_street`  DESC";

		$r = $mysqli->query($s);
		$prev = $r->fetch_assoc();
		if(preg_match("/-$/",$prev['txt'])){
			$txt = trim($prev['txt'],"- ") . "#" . $txt;
			continue;
		}else{
			$txt = trim($prev['txt']) . " " . "#" . $txt;
		}
		
		if($a = findnretc($txt)){
			//echo $txt . "\n";
			$yeah++;
			//print_r($a);
			$upd = "update observations set 
	            txt_lastname = '" . $mysqli->real_escape_string($a['name']) . "',
	            txt_street = '" . $mysqli->real_escape_string($a['street']) . "',
	            txt_initials = '" . $mysqli->real_escape_string($a['initials']) . "',
	            txt_number = '" . $mysqli->real_escape_string($a['huisnr']) . "'
	            where id = '" . $prev['id'] . "'";

	        if($run){
	            if (!$mysqli->query($upd)) {
	                printf("Error: %s\n", $mysqli->error);
	                echo $upd . ";\n";
	            }
	            echo "+ ";
	        }else{
	            echo $upd . ";\n";
	        }
		}else{
			echo "--- " . $txt . "\n";
			$nope++;
		}
		

		
	}
    

}
echo "\n" . $yeah . " yeahs en " . $nope . " nopes\n";

function findnretc($txt){

	$txt = trim(str_replace("#", "", $txt));

	$regex_huisnr = "[0-9]{1,3}((-|\/|, | en )[0-9]{1,3})?((-|\/|, )[0-9]{1,3})?[a-z]?\.?,?";
    
    if(preg_match("/^([^0-9\(\)]+) (" . $regex_huisnr . ")$/", $txt,$found)){
    	$parts = explode(" ",$found[1]);
    	$rparts = array_reverse($parts);
    	array_pop($parts);
    	$namepart = implode(" ", $parts);
    	$a = array(
    		"name" => $namepart,
    		"street" => $rparts[0],
    		"initials" => "",
    		"huisnr" => trim($found[2]," .,")
    	);
		return $a;
	}elseif(preg_match("/^([^\(]*) \(([^\)]*)\) ([^0-9\(\)]*) (" . $regex_huisnr . ")$/", $txt, $found)){
		$a = array(
    		"name" => $found[1],
    		"street" => $found[3],
    		"initials" => $found[2],
    		"huisnr" => trim($found[4]," .,")
    	);
    	return $a;
	}

	return false;

}



?>