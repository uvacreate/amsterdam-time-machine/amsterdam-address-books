<?php



include("../settings.php");

$run = true;

// read curated street list to array
$profcsv = '../../../../data/beroepen-standaard/beroepenlijst' . $jaar . 'plus.csv';
$profs = array();
if (($handle = fopen($profcsv, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 10000, ";")) !== FALSE) {

        if(!strlen($data[1])){
            continue;
        }
        
        $upd = "update observations set 
            profession = '" . $mysqli->real_escape_string($data[0]) . "'
            where txt_profession = '" . $mysqli->real_escape_string($data[1]) . "'";

        if($run){
            if (!$mysqli->query($upd)) {
                printf("Error: %s\n", $mysqli->error);
                echo $upd . ";\n";
            }
            echo "+ ";
        }else{
            echo $upd . ";\n";
        }

    }
    fclose($handle);
}


?>