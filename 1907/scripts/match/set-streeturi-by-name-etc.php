<?

include("../settings.php");

$run = true;

$sql = "SELECT * FROM `observations` 
		WHERE part = 'profession' 
		AND txt_lastname IS NOT NULL
		AND uri_street = ''
		ORDER BY `txt_lastname` ASC";

$result = $mysqli->query($sql);



while($original = $result->fetch_assoc()){ 

	

	$similar = array();
	
	// 1) Find same surname in this year
	$sql2 = "SELECT * FROM `observations` 
			WHERE txt_lastname ='" . $mysqli->real_escape_string($original['txt_lastname']) . "' 
			AND id <> " . $original['id'] . "
			AND uri_street <> ''";

	$result2 = $mysqli->query($sql2);


	$score = 0;
	while($other = $result2->fetch_assoc()){ 

		//print_r($other);
		if($original['txt_initials'] == $other['txt_initials'] && strlen($original['txt_initials'])){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

		if($original['uri_street'] == $other['uri_street'] && strlen($original['uri_street'])){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

		if($original['txt_number'] == $other['txt_number'] && strlen($original['txt_number'])){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

		if(levenshtein($original['txt_profession'],$other['txt_profession']) < 3 && strlen($original['txt_profession'])){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

		if(levenshtein($original['txt_street'],$other['txt_street']) < 3 && strlen($original['txt_street'])){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

	}

	

	// 2) Find same surname in next year
	$sql3 = "SELECT * FROM create_adresboeken_1909.`observations` 
			WHERE txt_lastname ='" . $mysqli->real_escape_string($original['txt_lastname']) . "' 
			AND id <> " . $original['id'] . "
			AND uri_street <> ''";

	$result3 = $mysqli->query($sql3);


	$score = 0;
	while($other = $result3->fetch_assoc()){ 

		//print_r($other);
		if($original['txt_initials'] == $other['txt_initials'] && strlen($original['txt_initials'])){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

		if($original['uri_street'] == $other['uri_street'] && strlen($original['uri_street'])){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

		if($original['txt_number'] == $other['txt_number'] && strlen($original['txt_number'])){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

		if(levenshtein($original['txt_profession'],$other['txt_profession']) < 3 && strlen($original['txt_profession'])){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

		if(levenshtein($original['txt_street'],$other['txt_street']) < 3 && strlen($original['txt_street'])){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

	}

	//print_r($similar);

	foreach ($similar as $k => $sim) {
		if($sim['score'] < 3){
			continue;
		}

		echo "\n########################################\n\n";

		echo $original['year'] . " | ";
		echo $original['txt_lastname'] . " | ";
		echo $original['txt_initials'] . " | ";
		echo $original['txt_givenname'] . " | ";
		echo $original['txt_street'] . " | ";
		echo $original['txt_number'] . " | ";
		echo $original['txt_profession'] . " | ";
		echo $original['uri_street'] . " | \n";

		echo "-----------------------------------------\n";
	
		echo $sim['data']['year'] . " | ";
		echo $sim['data']['txt_lastname'] . " | ";
		echo $sim['data']['txt_initials'] . " | ";
		echo $sim['data']['txt_givenname'] . " | ";
		echo $sim['data']['txt_street'] . " | ";
		echo $sim['data']['txt_number'] . " | ";
		echo $sim['data']['txt_profession'] . " | ";
		echo $sim['data']['uri_street'] . " | \n";

		$upd = "update observations set 
	            uri_street = '" . $mysqli->real_escape_string($sim['data']['uri_street']) . "'
	            where id = " . $original['id'];

	    if($run){
	        if (!$mysqli->query($upd)) {
	            printf("Error: %s\n", $mysqli->error);
	            echo $upd . ";\n";
	        }
	        echo "+ ";
	    }else{
	        echo $upd . ";\n";
	    }
	}


}


?>