<?


// didn't do a lot of good, this script - in here for documentational purposes only



include("../settings.php");

$run = false;

$sql = "SELECT * FROM `observations` 
		WHERE part = 'alphabetical' 
		AND txt_profession IS NOT NULL
		AND txt_lastname IS NOT NULL
		AND uri_street <> ''
		AND uri_street IS NOT NULL
		ORDER BY `txt_lastname` ASC";

$result = $mysqli->query($sql);



while($original = $result->fetch_assoc()){ 


	if(strlen($original['txt_lastname']) < 4){
		continue;
	}
	echo ". ";

	$similar = array();
	
	// 1) Find entries from profession list
	$sql2 = "SELECT * FROM `observations` 
			WHERE part = 'profession' AND txt_lastname IS NULL
			AND txt LIKE '" . $mysqli->real_escape_string( substr($original['txt_lastname'],0,5) ) . "%'
			AND txt LIKE '%" . $mysqli->real_escape_string( substr($original['txt_street'],0,5) ) . "%'
			";

	
	$result2 = $mysqli->query($sql2);


	$score = 0;
	while($other = $result2->fetch_assoc()){ 

		echo "| ";

		//print_r($other);
		if(strlen($original['txt_initials']) && strpos($other['txt'],$original['txt_initials']) !== false){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

		if(strlen($original['txt_number']) && strpos($other['txt'],$original['txt_number']) !== false){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

		if(strlen($original['txt_street']) && strpos($other['txt'],$original['txt_street']) !== false){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

		if(strlen($original['txt_lastnameprefix']) && strpos($other['txt'],$original['txt_lastnameprefix']) !== false){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

		if(levenshtein($original['txt_profession'],$other['txt_profession']) < 3 && strlen($original['txt_profession'])){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

	}

	/*
	// 2) Find same surname in previous year
	$sql3 = "SELECT * FROM create_adresboeken_1905.`observations` 
			WHERE txt_lastname ='" . $mysqli->real_escape_string($original['txt_lastname']) . "' 
			AND id <> " . $original['id'] . "
			AND uri_street <> ''";

	$result3 = $mysqli->query($sql3);


	$score = 0;
	while($other = $result3->fetch_assoc()){ 

		//print_r($other);
		if($original['txt_initials'] == $other['txt_initials'] && strlen($original['txt_initials'])){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

		if($original['uri_street'] == $other['uri_street'] && strlen($original['uri_street'])){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

		if($original['txt_number'] == $other['txt_number'] && strlen($original['txt_number'])){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

		if(levenshtein($original['txt_profession'],$other['txt_profession']) < 3 && strlen($original['txt_profession'])){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

		if(levenshtein($original['txt_street'],$other['txt_street']) < 3 && strlen($original['txt_street'])){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

	}
	*/

	//print_r($similar);

	foreach ($similar as $k => $sim) {
		if($sim['score'] < 2){
			continue;
		}

		echo "\n########################################\n\n";

		echo $original['year'] . " | ";
		echo $original['txt_lastname'] . " | ";
		echo $original['txt_initials'] . " | ";
		echo $original['txt_givenname'] . " | ";
		echo $original['txt_street'] . " | ";
		echo $original['txt_number'] . " | ";
		echo $original['txt_profession'] . " | ";
		echo $original['uri_street'] . " | \n";

		echo "-----------------------------------------\n";
	
		echo $sim['data']['year'] . " | ";
		echo $sim['data']['txt'] . " | ";
		echo $sim['data']['txt_profession'] . " | \n";

		$upd = "update observations set 
	            uri_street = '" . $mysqli->real_escape_string($original['uri_street']) . "'
	            txt_lastname = '" . $mysqli->real_escape_string($original['txt_lastname']) . "'
	            txt_number = '" . $mysqli->real_escape_string($original['txt_number']) . "'
	            where id = " . $sim['data']['id'];

	    if($run){
	        if (!$mysqli->query($upd)) {
	            printf("Error: %s\n", $mysqli->error);
	            echo $upd . ";\n";
	        }
	        echo "+ ";
	    }else{
	        echo $upd . ";\n";
	    }
	}


}


?>