<?


// didn't do a lot of good, this script - in here for documentational purposes only


include("../settings.php");

$run = false;

$sql = "SELECT * FROM `observations` 
		WHERE part = 'profession' 
		AND txt_profession IS NOT NULL
		AND txt <> txt_profession
		AND txt_lastname IS NULL
		AND (uri_street = '' OR uri_street IS NULL)
		";

$result = $mysqli->query($sql);



while($original = $result->fetch_assoc()){ 


	
	echo ". ";

	$similar = array();
	
	// 2) Find same profession in previous year
	$sql3 = "SELECT * FROM create_adresboeken_1905.`observations` 
			WHERE txt_profession = '" . $mysqli->real_escape_string($original['txt_profession']) . "' 
			AND txt LIKE '" . substr($original['txt'],0,6) . "%'
			AND uri_street <> ''";

	$result3 = $mysqli->query($sql3);


	$score = 0;
	while($other = $result3->fetch_assoc()){ 

		//print_r($other);
		if($original['txt_initials'] == $other['txt_initials'] && strlen($original['txt_initials'])){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

		if($original['uri_street'] == $other['uri_street'] && strlen($original['uri_street'])){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

		if($original['txt_number'] == $other['txt_number'] && strlen($original['txt_number'])){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

		if(levenshtein($original['txt_profession'],$other['txt_profession']) < 3 && strlen($original['txt_profession'])){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

		if(levenshtein($original['txt_street'],$other['txt_street']) < 3 && strlen($original['txt_street'])){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

	}

	//print_r($similar);

	foreach ($similar as $k => $sim) {
		if($sim['score'] < 0){
			continue;
		}

		echo "\n########################################\n\n";

		echo $original['year'] . " | ";
		echo $original['txt'] . " | ";
		echo $original['uri_street'] . " | \n";

		echo "-----------------------------------------\n";
	
		echo $sim['data']['year'] . " | ";
		echo $sim['data']['txt_lastname'] . " | ";
		echo $sim['data']['uri_street'] . " | ";
		echo $sim['data']['txt_number'] . " | ";
		echo $sim['data']['txt'] . " | \n";

		$upd = "update observations set 
	            uri_street = '" . $mysqli->real_escape_string($original['uri_street']) . "'
	            txt_lastname = '" . $mysqli->real_escape_string($original['txt_lastname']) . "'
	            txt_number = '" . $mysqli->real_escape_string($original['txt_number']) . "'
	            where id = " . $sim['data']['id'];

	    if($run){
	        if (!$mysqli->query($upd)) {
	            printf("Error: %s\n", $mysqli->error);
	            echo $upd . ";\n";
	        }
	        echo "+ ";
	    }else{
	        //echo $upd . ";\n";
	    }
	}


}


?>