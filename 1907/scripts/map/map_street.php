<?

include("../settings.php");

$run = false;

$street = "https://adamlink.nl/geo/street/jodenbreestraat/2158";
//$street = "https://adamlink.nl/geo/street/plantage-lijnbaansgracht/8174";
$street = "https://adamlink.nl/geo/street/derde-oosterparkstraat/972";
//$street = "https://adamlink.nl/geo/street/uilenburgerstraat/5667";
$street = "https://adamlink.nl/geo/street/groenburgwal/1564";
$street = "https://adamlink.nl/geo/street/staalstraat/4282";
$street = "https://adamlink.nl/geo/street/zanddwarsstraat/5141";

// read locatiepunten of current street
$lps = array();
$csv = '../../../../data/locatiepunten/locatiepunten.csv';

/* DIY: csv is result of following query on https://druid.datalegend.net/LvanWissen/locatiepunten/sparql/locatiepunten :
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX sem: <http://semanticweb.cs.vu.nl/2009/11/sem/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX lpont: <http://resolver.clariah.org/hisgis/lp/ontology/>
SELECT * WHERE {
  ?adres lpont:straat ?adamlinkuri .
  ?adres sem:hasLatestBeginTimeStamp "1909-12-31"^^xsd:date .
  ?adres lpont:huisnummer ?huisnr .
  ?adres skos:prefLabel ?adreslabel .
  ?adres geo:hasGeometry ?geom .
  ?geom geo:asWKT ?wkt .
  ?geom rdfs:label ?lpnr .
}
OFFSET 0 LIMIT 10000 
(update OFFSET to get all 48503 results)
*/

$beroepen = array();
if (($handle = fopen($csv, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 10000, ",")) !== FALSE) {
        if($street == $data[1]){
        	$lps[$data[2]] = $data; 
        }
    }
    fclose($handle);
}
//print_r($lps);
//die;


$sql = "SELECT * FROM `observations` 
		WHERE uri_street = '" . $street . "'
		AND part = 'streets'";

$result = $mysqli->query($sql);


$streetlist = array();

while($row = $result->fetch_assoc()){ 

	$streetlist[] = $row['txt_number'] . " | " . $row['txt'];

}

//print_r($streetlist);
//die;





$sql = "SELECT * FROM `observations` 
		WHERE uri_street = '" . $street . "'";

$result = $mysqli->query($sql);


$nrs = array();


while($row = $result->fetch_assoc()){ 

	
	$nr = (int)$row['number'];

	if($nr > 0){
		//$nrs[$nr][] = $row['txt'] . " | " . $row['txt_initials'] . " | " . $row['txt_lastname'] . " | " . $row['txt_profession'] . " | " . $row['part'];
		$nrs[$nr][] = $row['txt_lastname'] . " | " . $row['txt_profession'] . " | " . $row['part'];
	}

}

ksort($nrs);

//print_r($nrs);

$fc = array("type"=>"FeatureCollection", "features"=>array());

foreach ($nrs as $k => $v) {
	$adres = array("type"=>"Feature");
	$adres['geometry'] = wkt2geojson($lps[$k][5]);

	$props = array(
		"nummer" => $k,
		"op dit adres" => implode(" || ",$v)
	);

	$adres['properties'] = $props;
	$fc['features'][] = $adres;
}

$geojson = json_encode($fc);

echo $geojson;


function wkt2geojson($wkt){
	$coordsstart = strpos($wkt,"(");
	$type = trim(substr($wkt,0,$coordsstart));
	$coordstring = substr($wkt, $coordsstart);

	switch ($type) {
	    case "LINESTRING":
	    	$geom = array("type"=>"LineString","coordinates"=>array());
			$coordstring = str_replace(array("(",")"), "", $coordstring);
	    	$pairs = explode(",", $coordstring);
	    	foreach ($pairs as $k => $v) {
	    		$coords = explode(" ", trim($v));
	    		$geom['coordinates'][] = array((double)$coords[0],(double)$coords[1]);
	    	}
	    	return $geom;
	    	break;
	    case "POLYGON":
	    	$geom = array("type"=>"Polygon","coordinates"=>array());
			preg_match_all("/\([0-9. ,]+\)/",$coordstring,$matches);
	    	//print_r($matches);
	    	foreach ($matches[0] as $linestring) {
	    		$linestring = str_replace(array("(",")"), "", $linestring);
		    	$pairs = explode(",", $linestring);
		    	$line = array();
		    	foreach ($pairs as $k => $v) {
		    		$coords = explode(" ", trim($v));
		    		$line[] = array((double)$coords[0],(double)$coords[1]);
		    	}
		    	$geom['coordinates'][] = $line;
	    	}
	    	return $geom;
	    	break;
	    case "MULTILINESTRING":
	    	$geom = array("type"=>"MultiLineString","coordinates"=>array());
	    	preg_match_all("/\([0-9. ,]+\)/",$coordstring,$matches);
	    	//print_r($matches);
	    	foreach ($matches[0] as $linestring) {
	    		$linestring = str_replace(array("(",")"), "", $linestring);
		    	$pairs = explode(",", $linestring);
		    	$line = array();
		    	foreach ($pairs as $k => $v) {
		    		$coords = explode(" ", trim($v));
		    		$line[] = array((double)$coords[0],(double)$coords[1]);
		    	}
		    	$geom['coordinates'][] = $line;
	    	}
	    	return $geom;
	    	break;
	    case "POINT":
			$coordstring = str_replace(array("(",")"), "", $coordstring);
	    	$coords = explode(" ", $coordstring);
	    	//print_r($coords);
	    	$geom = array("type"=>"Point","coordinates"=>array((double)$coords[0],(double)$coords[1]));
	    	return $geom;
	        break;
	}
}




?>