# Adresboeken

The Amsterdam Adresboeken (addressbooks), as [found in the collections of the City Archives](https://archief.amsterdam/inventarissen/details/30274/path/63), provide insight in the occupations / professions of generations of Amsterdammers.

For now, this repository holds data extracted from the 1905-, 1907- , 1909- and 1910 Adresboeken. You'll find the extracted, splitted and cleaned data in the YEAR.csv files in each yearfolder.

## OCR-data, occupationlists, streetlists

In each yearfolder, you will find the ocr-data in the subfolder `data`. The `year_whole_fixed.txt` file contains the ocr-data of the whole adresboek.

The `beroepenlijstYEAR.csv` files hold all the occupations as extracted and cleaned from the ocr-data in an earlier process in the first field, and the exact ocrtext of the line where that occupation starts in the second. It is used to add the 'cleaned' occupation to the entry in `scripts/ins_beroepen.php`. For 1907, this list has been checked and cleaned manually, and the resulting list was the basis for [beroepen-hisco.csv](referencedata/beroepen-hisco.csv), in which the list is aligned with HISCO categories.

The `streetlist-YEAR.txt` files hold all the streetnames extracted from the streetlisting of that year. It is extracted by getting each line before a line starting with 'Buurt', and manually cleaned. It is used by `scripts/ins_streets.php` to add the streetname to the entry.

## Parts

Each adresboek consists of different listings (links to the 1905 Adresboek)

- [by occupation](https://archief.amsterdam/inventarissen/scans/30274/63/start/910/limit/10/highlight/8)
- [by street](https://archief.amsterdam/inventarissen/scans/30274/63/start/710/limit/10/highlight/9)
- [alphabetically](https://archief.amsterdam/inventarissen/scans/30274/63/start/70/limit/10/highlight/2)
- [studenten corps](https://archief.amsterdam/inventarissen/scans/30274/63/start/710/limit/10/highlight/4)

## Reference data

In the [referencedata folder](referencedata) you will find the following files:

- adamlink-matches-in-lp.csv - in the [locatiepunten set](https://druid.datalegend.net/LvanWissen/locatiepunten) not all addresses were properly linked to Adamlink street identifiers, this list partly makes up for that.
- beroepen-hisco.csv - the manually checked occupations mentioned in the 1907 'by occupation' section aligned to [HISCO](https://druid.datalegend.net/HistoryOfWork/historyOfWork-all-latest) categories
- ocr-variants.csv - variants of streetnames found in this project linked to Adamlink identifiers; these links were checked more or less manually: obvious incorrect links were deleted.
- lp-notes.md (in Dutch) contains some remarks on mismatches between HisGis locatiepunten and Adamlink streets and reasons they might have occurred.
- straatnamen-amsterdam.csv - namevariants of streets with links to Adamlink identifiers, data extracted from Adamlink


## Data cleaning and alignment

Datacleaning is an ongoing process, and never 'done'. Thus not all ocr-text has been split into parts, not all streetnames have been aligned with Adamlink URIs. Yet.

In the `scripts` folders, you'll find scripts to perform the following actions:

1. read ocr-data into MySQL table [observations](1905/scripts/observations.sql)
2. split ocr-text into meaningful parts: surname, given name, street, profession, etc.
3. align streets with Adamlink URIs 
4. find persons and companies - both in other parts of this years adresboek and in previous and next years - to validate housenumbers, align streetnames, etc.
5. align addresses with HisGis 'locatiepunten'
6. align professions through normalised professions with HISCO 

## YEAR.csv

The `YEAR.csv` file in each yearfolder is a csv-dump of the MySQL table. It contains the ocr-data, the splitted parts of the ocr-data (surname, initials, address, etc.) in fields starting with `txt_`, and the cleaned / aligned data in fields without that prefix. 

The `YEAR.sql` contains the same data, as sql.

## Map app 1907

On [addressbooks.amsterdamtimemachine.nl/](http://addressbooks.amsterdamtimemachine.nl/) a map application shows the listings of the 1907 Adresboek. The [code](https://github.com/mmmenno/adresboek) could be used for other years as well.

## TODO

For those in search for cleaning & alignment jobs:

- Only professions in the 1907 'per profession' listings were manually checked and normalised (and through the normalised list linked to HISCO). The 1905, 1909 and 1910 adresboeken need some work on that.
- For all years, only professions in the 'per profession' listings were normalised, while professions also occur in alphabetical listings (in all kinds of ocr-variants). Some professions (letterzetter) occur in the alphabetical listings only.
- Matching adresboek entries to persons in other datasets (e.g. Bevolkingsregister) could be a way to track down ocr-mistakes and improve the adresboek data.
- In the end, to get a 100 percent accuracy, editing entries manually is necessary. Some ocr-mistakes and poorly splitted entries cannot be solved otherwise.
- There are still plenty of streetnames (or rather, ocrvariants of streetnames) that could be aligned with Adamlink streets. My workflow: find candidates with `levenshtein-straten.php`, check results manually and add them to `ocr-variants.csv`, then run `align-straten.php`
- As HISCO only considers shopowners or traders as such, it might be nice to match the products they sold / traded in to Wikidata, AAT or the Cultural Heritage Thesaurus.

Of course, every time the data has been cleaned or aligned somewhat further, the scripts in the `match` folders could be run to find and correct previously unnoticed ocr-mistakes. 


## Possibilities for future research

- The members of the _Amsterdamsch Studenten Corps_ are listed seperately - where did they live and what were their occupations?
- Widows are (often) referred to as 'Wed.', idem. Also, misses (Mej.).
- Not all inhabitants of the city are listed - compare with other sources (woningkaarten, bevolkingsregister) for a specific street to get a grip on who are and who are not.


