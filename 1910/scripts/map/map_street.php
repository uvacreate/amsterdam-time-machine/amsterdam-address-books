<?

include("../settings.php");

$run = false;

$street = "https://adamlink.nl/geo/street/jodenbreestraat/2158";
//$street = "https://adamlink.nl/geo/street/plantage-lijnbaansgracht/8174";
//$street = "https://adamlink.nl/geo/street/derde-oosterparkstraat/972";
//$street = "https://adamlink.nl/geo/street/uilenburgerstraat/5667";

// read locatiepunten of current street
$lps = array();
$csv = '../../../../data/locatiepunten/locatiepunten.csv';

/* DIY: csv is result of following query on https://druid.datalegend.net/LvanWissen/locatiepunten/sparql/locatiepunten :
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX sem: <http://semanticweb.cs.vu.nl/2009/11/sem/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX lpont: <http://resolver.clariah.org/hisgis/lp/ontology/>
SELECT * WHERE {
  ?adres lpont:straat ?adamlinkuri .
  ?adres sem:hasLatestBeginTimeStamp "1909-12-31"^^xsd:date .
  ?adres lpont:huisnummer ?huisnr .
  ?adres skos:prefLabel ?adreslabel .
  ?adres geo:hasGeometry ?geom .
  ?geom geo:asWKT ?wkt .
  ?geom rdfs:label ?lpnr .
}
OFFSET 0 LIMIT 10000 
(update OFFSET to get all 48503 results)
*/

$beroepen = array();
if (($handle = fopen($csv, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 10000, ",")) !== FALSE) {
        if($street == $data[1]){
        	$lps[$data[2]] = $data; 
        }
    }
    fclose($handle);
}
print_r($lps);
//die;


$sql = "SELECT * FROM `observations` 
		WHERE uri_street = '" . $street . "'
		AND part = 'streets'";

$result = $mysqli->query($sql);


$streetlist = array();

while($row = $result->fetch_assoc()){ 

	$streetlist[] = $row['txt_number'] . " | " . $row['txt'];

}

print_r($streetlist);
die;





$sql = "SELECT * FROM `observations` 
		WHERE uri_street = '" . $street . "'";

$result = $mysqli->query($sql);


$nrs = array();

while($row = $result->fetch_assoc()){ 

	$nr = (int)$row['number'];

	if($nr > 0){
		$nrs[$nr][] = $row['txt'] . " | " . $row['txt_initials'] . " | " . $row['txt_lastname'] . " | " . $row['txt_profession'] . " | " . $row['part'];
	}

}

ksort($nrs);

print_r($nrs);


?>