<?

include("settings.php");

$run = false;



$sql = "SELECT * FROM observations
        WHERE year = " . $jaar  . "
        AND part = 'alphabetical'
        AND txt_lastname is null
        ORDER BY id ASC";
$result = $mysqli->query($sql);


while($row = $result->fetch_assoc()){

    $txt = trim($row['txt'],',.‚| ');
    //echo $row['txt'] . "\n";
    //echo $txt . "\n";

    $regex_huisnr = "[0-9]{1,4}(-[0-9]{1,3})?[a-z]?";
    $regex_initial = "([A-Z]\.?|Fr\.?|Alb\.?|Alph\.?|El\.?|Ch\.?|Chr\.?|Joh\.?|Th\.?|Corn\.?|Ph\.?|Jac\.?|Ed\.?|Fl\.?|Abr\.?|Aug\.?|Herm\.?|Jos\.?|Ant\.?)";
    $regex_initials = $regex_initial . " ?" . $regex_initial . "? ?" . $regex_initial . "? ?" . $regex_initial . "? ?" . $regex_initial . "? ?" . $regex_initial . "? ?";
    $regex_tussenv = "(van|de|van de|van der|van den|den|ten|v\. ?d\.|v\.d|v\.|la|de la|vander|vanden|von der)";
    $regex_surname = "[A-Za-zéèüáàïöë\-\.]+";
    $regex_title = "(Mej|Wed|Dr|Mevr)";

    $entity_type = "";
    $prefix = "";
    $lastname = "";
    $initials = "";
    $givenname = "";
    $number = "";
    $street = "";
    $lastnameprefix = "";
    $prof = "";


    if(preg_match("/^(" . $regex_surname . ") (\(|,|{|«|‘|:|;)((" . $regex_initials . ") ?" . $regex_tussenv . "?)\) (.*) (" . $regex_huisnr . ")(.*) Tel\. [0-9]{2,6}$/", $txt,$found)){
        // Haasbergen (L. van) Keizersgracht, 56. Agent van Buitenl. Huizen. Tel. 5504
        //echo $txt . "\n";
        //print_r($found);
        $lastname = trim($found[1],',.‚| ');
        $initials = trim($found[4]);
        $number = trim($found[13]);
        $street = trim($found[12]);
        $lastnameprefix = trim($found[11]);
        $prof = trim($found[15],',.‚| ');
        //continue;
    }elseif(preg_match("/^(" . $regex_surname . ") (\(|,|{|«|‘|:|;)((" . $regex_initials . ") ?" . $regex_tussenv . "?)\) (.*) (" . $regex_huisnr . ")$/", $txt,$found)){
        //echo $txt . "\n";
        //print_r($found);
        $lastname = trim($found[1],',.‚| ');
        $initials = trim($found[4]);
        $number = trim($found[13]);
        $street = trim($found[12],',.‚| ');
        $lastnameprefix = trim($found[11]);
        $entity_type = "person";
        //continue;
    }elseif(preg_match("/^(" . $regex_surname . ") (\(|,|{|«|‘|:|;)(" . $regex_initials . ") " . $regex_tussenv . "?\) (.*) (" . $regex_huisnr . ")(.*)$/", $txt,$found)){
        //echo $txt . "\n";
        //print_r($found);
        $lastname = trim($found[1],',.‚| ');
        $lastnameprefix = trim($found[10]);
        $number = trim($found[12]);
        $street = trim($found[11],',.‚| ');
        $prof = trim($found[14],',.‚| ');
        $initials = $found[3];
        //continue;
    }elseif(preg_match("/^([A-Z].*) (\(|,|{|«|‘|:|;)(.*) " . $regex_tussenv . "?\) (.*) ([0-9]{1,4})$/", $txt,$found)){
        //echo $txt . "\n";
        //print_r($found);
        $lastname = trim($found[1],',.‚| ');
        $lastnameprefix = trim($found[4]);
        $number = trim($found[6]);
        $street = trim($found[5],',.‚| ');
        if(preg_match("/Mej/", $found[3])){
            $prefix = "Mej.";
            $initials = trim(str_replace(array("Mej.","Mej"),"",$found[3]));
        }elseif(preg_match("/Wed/", $found[3])){
            $prefix = "Wed.";
            $initials = trim(str_replace(array("Wed.","Wed"),"",$found[3]));
        }else{
            $initials = $found[3];
        }
        $entity_type = "person";
        //continue;
    }elseif(preg_match("/^([A-Z].*) (\(|,|{|«|‘|:|;)(.*) " . $regex_tussenv . "?\) (.*) ([0-9]{1,4})(.*)$/", $txt,$found)){
        //echo $txt . "\n";
        //print_r($found);
        $lastname = trim($found[1],',.‚| ');
        $lastnameprefix = trim($found[4]);
        $number = trim($found[6]);
        $street = trim($found[5],',.‚| ');
        $prof = trim($found[7],',.‚| ');
        if(preg_match("/Mej/", $found[3])){
            $prefix = "Mej.";
            $initials = trim(str_replace(array("Mej.","Mej"),"",$found[3]));
        }elseif(preg_match("/Wed/", $found[3])){
            $prefix = "Wed.";
            $initials = trim(str_replace(array("Wed.","Wed"),"",$found[3]));
        }else{
            $initials = $found[3];
        }
        $entity_type = "person";
        //continue;
    }elseif(preg_match("/^([A-Z].*) (\(|,|{|«|‘|:|;)(.*)\) (.*) ([0-9]{1,4})(.*) Tel\. [0-9]{2,6}$/", $txt,$found)){
        //echo $txt . "\n";
        //print_r($found);
        $lastname = trim($found[1],',.‚| ');
        $number = trim($found[5]);
        $street = trim($found[4],',.‚| ');
        $prof = trim($found[6],',.‚| ');
        if(preg_match("/Mej/", $found[3])){
            $prefix = "Mej.";
            $initials = trim(str_replace(array("Mej.","Mej"),"",$found[3]));
        }elseif(preg_match("/Wed/", $found[3])){
            $prefix = "Wed.";
            $initials = trim(str_replace(array("Wed.","Wed"),"",$found[3]));
        }elseif(preg_match("/[a-z]{2,10}/", $found[3])){
            $givenname = trim($found[3]);
        }else{
            $initials = $found[3];
        }
        //continue;
    }elseif(preg_match("/^([A-Z].*) (\(|,|{|«|‘|:|;)(.*)\) (.*) ([0-9]{1,4})(.*)$/", $txt,$found)){
        //echo $txt . "\n";
        //print_r($found);
        $lastname = trim($found[1],',.‚| ');
        $number = trim($found[5]);
        $street = trim($found[4],',.‚| ');
        $prof = trim($found[6],',.‚| ');
        if(preg_match("/Mej/", $found[3])){
            $prefix = "Mej.";
            $initials = trim(str_replace(array("Mej.","Mej"),"",$found[3]));
        }elseif(preg_match("/Wed/", $found[3])){
            $prefix = "Wed.";
            $initials = trim(str_replace(array("Wed.","Wed"),"",$found[3]));
        }elseif(preg_match("/[a-z]{2,10}/", $found[3])){
            $givenname = trim($found[3]);
        }else{
            $initials = $found[3];
        }
        //continue;
    }else{
        //echo $txt . "\n";
        //echo "- ";
        continue;
    }
    
    //continue;
    

    $upd = "update observations set 
            txt_lastname = '" . $mysqli->real_escape_string($lastname) . "',
            txt_initials = '" . $mysqli->real_escape_string($initials) . "',
            txt_givenname = '" . $mysqli->real_escape_string($givenname) . "',
            txt_street = '" . $mysqli->real_escape_string(utf8_encode($street)) . "',
            txt_number = '" . $mysqli->real_escape_string($number) . "',
            txt_prefix = '" . $mysqli->real_escape_string($prefix) . "',
            txt_lastnameprefix = '" . $mysqli->real_escape_string($lastnameprefix) . "',
            txt_profession = '" . $mysqli->real_escape_string(utf8_encode($prof)) . "',
            entity_type = '" . $mysqli->real_escape_string($entity_type) . "'
            where id = " . $row['id'];

    if($run){
        if (!$mysqli->query($upd)) {
            printf("Error: %s\n", $mysqli->error);
            echo $upd . ";\n";
        }
        echo ". ";
    }else{
        echo $upd . ";\n";
    }
    
}











?>