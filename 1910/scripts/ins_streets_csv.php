<?

include("settings.php");

$run = true;

$txt = '../data/' . $jaar . '_alpha_met_bbox_fromnas.txt';


// read curated street list to array
$streetcsv = '../data/streetlist-' . $jaar . '.txt';
$streets = array();
if (($handle = fopen($streetcsv, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 10000, ";")) !== FALSE) {
        
        $fields = str_getcsv($data[0],"#####");
        $streets[] = $fields[0];

    }
    fclose($handle);
}
//print_r($streets);
//die;

$i = 0;
$lastkey = -2;
$street = "Achterburgwal.";

$csv = '../data/' . $jaar . '_whole_fixed.csv';

$i = 0;

if (($handle = fopen($csv, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
        //print_r($data);

        $line = $data[0];
        
        $i++;

        if($i < 87722){     // startline streets section in file
            continue;
        }
        if($i == 160687){   // end of the streets section
            die;
        }
        
        if(preg_match("/^B(uu|auau)rt/", $line)){

            $key = array_search($lastline, $streets);
            
            if($key !== false){
                $dif = abs($lastkey - $key);
                if($dif < 10){
                    echo "\nnew street: " . $key . " - " . $lastline;
                    $street = trim($lastline);
                    $lastkey = $key;
                }else{
                    echo "\njust a corner: ----- " . $key . " - " . $lastline;
                }
            }
            
        }
        //echo "=== " . $line;

        $lastline = $line;




        // skip some lines that are obviously nonhuman
        if(preg_match("/^Buurt /", $line)){
            continue;
        }
        if(preg_match("/^Kiesdistrict /", $line)){
            continue;
        }
        if(preg_match("/^hoek /", $line)){
            continue;
        }
        if(preg_match("/^" . preg_quote($street) . "/", $line)){
            continue;
        }
        



        $inssql = "insert into observations (scan,bbox,year,part,txt,txt_street)
                    VALUES (
                        '" . $mysqli->real_escape_string(trim($data[2])) . "',
                        '" . $mysqli->real_escape_string(trim($data[1])) . "',
                        " . $jaar . ",
                        'streets',
                        '" . $mysqli->real_escape_string(trim($line)) . "',
                        '" . $mysqli->real_escape_string(trim($street)) . "'
                    )";

        if($run){
            if (!$mysqli->query($inssql)) {
                printf("Error: %s\n", $mysqli->error);
                echo $inssql . ";\n";
            }
            echo ". ";
        }else{
            echo $inssql . ";\n";
        }

    }
    fclose($handle);
}






?>