<?

include("../settings.php");

$run = false;

$namescsv = "../../../referencedata/straatnamen-amsterdam.csv"; 	// Adamlink data
//$namescsv = "../../../referencedata/ocr-variants.csv";			// earlier matched ocr-variants in this project

$sql = "SELECT DISTINCT(txt_street), COUNT(*) as nr FROM `observations` 
		WHERE txt_street <> '' AND uri_street = ''
		group by txt_street ORDER BY `nr` DESC";

$result = $mysqli->query($sql);


$straatnamen = array();
if (($handle = fopen($namescsv, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 5000, ",")) !== FALSE) {
    	//print_r($data);
    	if($data[5] > 1920){
    		continue;
    	}
        $straatnamen[$data[0]] = $data[1];
    }
    fclose($handle);
}

while($row = $result->fetch_assoc()){ 

	//print_r($row);
	$uri = "";
	$street = trim($row['txt_street'],',.‚ ');
	//echo $street . "\n";

	if(isset($straatnamen[$street])){
		$uri = $straatnamen[$street];
		//continue;
	}else{
		//echo "- ";
		continue;
	}
	


	$upd = "update observations set 
			uri_street = '" . $uri . "'
			where txt_street = '" . $mysqli->real_escape_string($row['txt_street']) . "'
			and uri_street = ''";

	if($run){
        if (!$mysqli->query($upd)) {
            printf("Error: %s\n", $mysqli->error);
            echo $upd . ";\n";
        }
        echo "+ ";
    }else{
        echo $upd . ";\n";
    }
}


?>