<?

include("../settings.php");

$run = true;

$sql = "SELECT * FROM `observations` 
		WHERE txt_lastname IS NOT NULL
		AND uri_street <> ''
		AND (number is not null AND number <> '')
		ORDER BY `txt_lastname` ASC";

$result = $mysqli->query($sql);



while($original = $result->fetch_assoc()){ 

	$similar = array();
	
	// 1) Find same surname in this year
	$sql2 = "SELECT * FROM `observations` 
			WHERE id <> " . $original['id'] . "
			AND uri_street = '" . $original['uri_street'] . "'
			AND (number = '" . $original['number'] . "' OR txt_number = '" . $original['number'] . "')";

	$result2 = $mysqli->query($sql2);


	$score = 0;
	while($other = $result2->fetch_assoc()){ 

		//print_r($other);
		if($original['txt_initials'] == $other['txt_initials'] && strlen($original['txt_initials'])){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

		if(levenshtein($original['txt_lastname'],$other['txt_lastname']) < 3 && strlen($original['txt_lastname'])){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

	}

	

	

	// 3) Find same surname in prev year
	$sql3 = "SELECT * FROM create_adresboeken_1909.`observations` 
			WHERE id <> " . $original['id'] . "
			AND uri_street = '" . $original['uri_street'] . "'
			AND (number = '" . $original['number'] . "' OR txt_number = '" . $original['number'] . "')";

	$result3 = $mysqli->query($sql3);


	$score = 0;
	while($other = $result3->fetch_assoc()){ 

		//print_r($other);
		if($original['txt_initials'] == $other['txt_initials'] && strlen($original['txt_initials'])){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

		if(levenshtein($original['txt_lastname'],$other['txt_lastname']) < 3 && strlen($original['txt_lastname'])){
			$similar[$other['year'] . "-" . $other['id']]['score']++;
			$similar[$other['year'] . "-" . $other['id']]['data'] = $other;
		}

	}

	//print_r($similar);
	$names = array();

	$names[$original['txt_lastname']] = 1;
	foreach ($similar as $k => $sim) {
		if($sim['score'] < 2){
			continue;
		}
		$names[$sim['data']['txt_lastname']]++;
	}

	asort($names,SORT_NUMERIC); 				// sorted by nr of occurrences asc

	foreach ($similar as $k => $sim) {
		if($sim['score'] < 2){
			continue;
		}

		foreach ($names as $key => $value) {
			$firstname = $key;					// looping, so last occurred most
		}

		if($names[$firstname]<2){				// discard names that occurred only once 
			continue;
		}
		
		echo "\n########################################\n\n";

		echo $original['year'] . " | ";
		echo $original['txt_lastname'] . " | ";
		echo $original['txt_initials'] . " | ";
		echo $original['txt_givenname'] . " | ";
		echo $original['txt_street'] . " | ";
		echo $original['txt_number'] . " | ";
		echo $original['txt_profession'] . " | ";
		echo $original['uri_street'] . " | \n";

		echo "-----------------------------------------\n";
	
		echo $sim['data']['year'] . " | ";
		echo $sim['data']['txt_lastname'] . " | ";
		echo $sim['data']['txt_initials'] . " | ";
		echo $sim['data']['txt_givenname'] . " | ";
		echo $sim['data']['txt_street'] . " | ";
		echo $sim['data']['txt_number'] . " | ";
		echo $sim['data']['txt_profession'] . " | ";
		echo $sim['data']['uri_street'] . " | \n";

		print_r($names);

		$upd = "update observations set 
	            lastname = '" . $mysqli->real_escape_string($firstname) . "'
	            where id = " . $original['id'];

	    if($run){
	        if (!$mysqli->query($upd)) {
	            printf("Error: %s\n", $mysqli->error);
	            echo $upd . ";\n";
	        }
	        echo "+ ";
	    }else{
	        echo $upd . ";\n";
	    }

	}
	


}


?>