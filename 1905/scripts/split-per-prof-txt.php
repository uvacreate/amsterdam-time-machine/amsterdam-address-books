<?

include("settings.php");

$run = false;



$sql = "SELECT * FROM observations
        WHERE year = " . $jaar  . "
        AND part = 'profession'
        AND txt_lastname IS NULL
        ORDER BY id ASC";
$result = $mysqli->query($sql);


while($row = $result->fetch_assoc()){

    $txt = trim($row['txt'],',.‚| ');
    $txt = str_replace(",",".",$txt);
    $txt = str_replace("?","",$txt);

    
    if(preg_match("/-$/", $txt)){ // glue next line, don't do this the first run!

        $next = $row['id']+1;
        $s = "select * from observations where id = " . $next;
        $r = $mysqli->query($s);
        $r2 = $r->fetch_assoc();

        $txt = substr($txt,0,-1) . "" . $r2['txt'];

    }
    
    

    //echo $row['txt'] . "\n";
    //echo $txt . "\n";

    $regex_huisnr = "[0-9]{1,4}(-[0-9]{1,3})?[a-z]?";
    $regex_initial = "([A-Z]\.?|Fr\.?|Alb\.?|Alph\.?|El\.?|Ch\.?|Chr\.?|Joh\.?|Th\.?|Corn\.?|Ph\.?|Jac\.?|Ed\.?|Fl\.?|Abr\.?|Aug\.?|Herm\.?|Jos\.?|Ant\.?)";
    $regex_initials = $regex_initial . " ?" . $regex_initial . "? ?" . $regex_initial . "? ?" . $regex_initial . "? ?" . $regex_initial . "? ?" . $regex_initial . "? ?";
    $regex_tussenv = "(van|de|van de|van der|van den|den|ten|v\. ?d\.|v\.d|v\.|la|de la|vander|vanden|von der)";
    $regex_surname = "[A-Za-zéèüáàïöë\-\.]+";
    $regex_title = "(Mej|Wed|Dr|Mevr)";

    $entity_type = "";
    $prefix = "";
    $initials = "";
    $givenname = "";
    $lastname = "";
    $number = "";
    $street = "";
    $lastnameprefix = "";

    if(preg_match("/^(" . $regex_surname . ") \((" . $regex_initials . ") ?" . $regex_tussenv . "?\) (.*) (" . $regex_huisnr . ")$/", $txt,$found)){
        //echo $txt . "\n";
        //print_r($found);
        $lastname = trim($found[1],',.‚| ');
        $number = trim($found[11]);
        $street = trim($found[10],',.‚| ');
        $initials = trim($found[2]);
        $lastnameprefix = trim($found[9]);
        $entity_type = "person";
        //continue;
    }elseif(preg_match("/^(" . $regex_surname . ") \(" . $regex_title . "\.? ?(" . $regex_initials . ")? ?" . $regex_tussenv . "?\) (.*) (" . $regex_huisnr . ")$/", $txt,$found)){
        //echo $txt . "\n";
        //print_r($found);
        $lastname = trim($found[1],',.‚| ');
        $number = trim($found[12]);
        $street = trim($found[11],',.‚| ');
        $initials = trim($found[3]);
        $lastnameprefix = trim($found[10]);
        $prefix = $found[2];
        $entity_type = "person";
        //continue;
    }elseif(preg_match("/^(" . $regex_surname . ") \(Firma ?(" . $regex_initials . ")? ?" . $regex_tussenv . "?\) (.*) (" . $regex_huisnr . ")$/", $txt,$found)){
        //echo $txt . "\n";
        //print_r($found);
        $lastname = trim($found[1],',.‚| ');
        $number = trim($found[11]);
        $street = trim($found[10],',.‚| ');
        $initials = trim($found[2]);
        $lastnameprefix = trim($found[9]);
        $prefix = "";
        $entity_type = "firma";
        //continue;
    }elseif(preg_match("/^(" . $regex_surname . ") \(((Gebr|Gebrs|Gez|Gezs|Dames)\.? ?" . $regex_tussenv . "?)\) (.*) (" . $regex_huisnr . ")$/", $txt,$found)){
        //echo $txt . "\n";
        //print_r($found);
        $lastname = trim($found[1],',.‚| ');
        $number = trim($found[6]);
        $street = trim($found[5],',.‚| ');
        $lastnameprefix = trim($found[4]);
        $prefix = trim($found[3]);
        $entity_type = "gebr./gez.";
        //continue;
    }elseif(preg_match("/^(" . $regex_surname . ") \((([A-Z][a-z]+) ?" . $regex_tussenv . "?)\) (.*) (" . $regex_huisnr . ")$/", $txt,$found)){
        //echo $txt . "\n";
        //print_r($found);
        $lastname = trim($found[1],',.‚| ');
        $number = trim($found[6]);
        $street = trim($found[5],',.‚| ');
        $givenname = trim($found[3]);
        $lastnameprefix = trim($found[4]);
        $entity_type = "person";
        //continue;
    }elseif(preg_match("/^(" . $regex_surname . ") (& Co\.?|en Co\.?) \((" . $regex_initials . ") ?" . $regex_tussenv . "?\) (.*) (" . $regex_huisnr . ")$/", $txt,$found)){
        //echo $txt . "\n";
        //print_r($found);
        $lastname = trim($found[1],',.‚| ');
        $number = trim($found[12]);
        $street = trim($found[11],',.‚| ');
        $initials = trim($found[3]);
        $lastnameprefix = trim($found[10]);
        $entity_type = "& Co";
        //continue;
    }elseif(preg_match("/^(" . $regex_surname . ") (en|&) ?(Zn\.?|Son\.?|Zoon\.?|Zonen\.?|Zu\.?) \((" . $regex_initials . ") ?" . $regex_tussenv . "?\) (.*) (" . $regex_huisnr . ")$/", $txt,$found)){
        //echo $txt . "\n";
        //print_r($found);
        $lastname = trim($found[1],',.‚| ');
        $number = trim($found[13]);
        $street = trim($found[12],',.‚| ');
        $initials = trim($found[4]);
        $lastnameprefix = trim($found[11]);
        $entity_type = "& Zoon";
        //continue;
    }elseif(preg_match("/^(.+) \((" . $regex_initials . ") ?" . $regex_tussenv . "?\) (.*) (" . $regex_huisnr . ")$/", $txt,$found)){
        //echo $txt . "\n";
        //print_r($found);
        $lastname = trim($found[1],',.‚| ');
        $number = trim($found[11]);
        $street = trim($found[10],',.‚| ');
        $initials = trim($found[2]);
        $lastnameprefix = trim($found[9]);
        $entity_type = "person";
        //continue;
    }elseif(preg_match("/^(.+) \((.+)\)(.*) (" . $regex_huisnr . ")$/", $txt,$found)){
        //echo $txt . "\n";
        //print_r($found);
        $lastname = trim($found[1],',.‚| ');
        $number = trim($found[4]);
        $street = trim($found[3],',.‚| ');
        //continue;
    }elseif(preg_match("/^(.+) \((" . $regex_initials . ") ?" . $regex_tussenv . "?\) (.*) (" . $regex_huisnr . ")/", $txt,$found)){
        //echo $txt . "\n";
        //print_r($found);
        $lastname = trim($found[1],',.‚| ');
        $number = trim($found[11]);
        $street = trim($found[10],',.‚| ');
        $initials = trim($found[2]);
        $lastnameprefix = trim($found[9]);
        $entity_type = "person";
        //continue;
    }elseif(preg_match("/^(.+) \((.+)\) (.*) (" . $regex_huisnr . ")/", $txt,$found)){
        //echo $txt . "\n";
        //print_r($found);
        $lastname = trim($found[1],',.‚| ');
        $number = trim($found[4]);
        $street = trim($found[3],',.‚| ');
        //continue;
    }elseif(preg_match("/^([^\(]{5,30})\. (.*) (" . $regex_huisnr . ")$/", $txt,$found)){
        //echo $txt . "\n";
        //print_r($found);
        $lastname = trim($found[1],',.‚| ');
        $number = trim($found[3]);
        $street = trim($found[2],',.‚| ');
        //continue;
    }elseif(preg_match("/^(" . $regex_surname . ") \((" . $regex_initials . ") ?" . $regex_tussenv . "?\)/", $txt,$found)){
        //echo $txt . "\n";
        //print_r($found);
        $lastname = trim($found[1],',.‚| ');
        $lastnameprefix = trim($found[9]);
        $initials = trim($found[2]);
        //continue;
    }else{
        //echo $txt . "\n";
        //echo "- ";
        continue;
    }
    
    //continue;
    

    $upd = "update observations set 
            txt_lastname = '" . $mysqli->real_escape_string(utf8_encode($lastname)) . "',
            txt_initials = '" . $mysqli->real_escape_string($initials) . "',
            txt_givenname = '" . $mysqli->real_escape_string($givenname) . "',
            txt_street = '" . $mysqli->real_escape_string(utf8_encode($street)) . "',
            txt_number = '" . $mysqli->real_escape_string($number) . "',
            txt_prefix = '" . $mysqli->real_escape_string($prefix) . "',
            txt_lastnameprefix = '" . $mysqli->real_escape_string($lastnameprefix) . "',
            entity_type = '" . $mysqli->real_escape_string($entity_type) . "'
            where id = " . $row['id'];

    if($run){
        if (!$mysqli->query($upd)) {
            printf("Error: %s\n", $mysqli->error);
            echo $upd . ";\n";
        }
        echo "+ ";
    }else{
        echo $upd . ";\n";
    }
    
}











?>