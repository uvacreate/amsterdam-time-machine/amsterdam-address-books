-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Gegenereerd op: 26 apr 2021 om 23:32
-- Serverversie: 5.7.26
-- PHP-versie: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `create_adresboeken_1905`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `observations`
--

CREATE TABLE `observations` (
  `id` int(11) NOT NULL,
  `scan` varchar(100) DEFAULT NULL,
  `bbox` varchar(100) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `part` varchar(100) DEFAULT NULL,
  `txt` text,
  `txt_lastnameprefix` varchar(100) DEFAULT NULL,
  `txt_lastname` varchar(255) DEFAULT NULL,
  `txt_initials` varchar(255) DEFAULT NULL,
  `txt_givenname` varchar(255) DEFAULT NULL,
  `txt_prefix` varchar(100) DEFAULT NULL,
  `txt_street` varchar(255) DEFAULT NULL,
  `txt_number` varchar(255) DEFAULT NULL,
  `number` varchar(100) DEFAULT NULL,
  `txt_profession` varchar(255) DEFAULT NULL,
  `uri_street` varchar(255) DEFAULT '',
  `geojson` text,
  `lp` varchar(255) DEFAULT NULL,
  `is_observation` varchar(100) DEFAULT NULL,
  `entity_type` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `observations`
--
ALTER TABLE `observations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uri_street` (`uri_street`),
  ADD KEY `txt_street` (`txt_street`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `observations`
--
ALTER TABLE `observations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
