<?

include("../settings.php");

$run = false;

$namescsv = "../../../referencedata/straatnamen-amsterdam.csv"; 	// Adamlink data
//$namescsv = "../../../referencedata/ocr-variants.csv";			// earlier matched ocr-variants in this project

$sql = "SELECT DISTINCT(txt_street), COUNT(*) as nr FROM `observations` 
		WHERE txt_street <> '' AND uri_street = ''
		group by txt_street ORDER BY `nr` DESC";

$result = $mysqli->query($sql);


$straatnamen = array();
if (($handle = fopen($namescsv, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 5000, ",")) !== FALSE) {
    	//print_r($data);
    	if($data[5] > 1920){				// do not use more recent streets
    		continue;
    	}
        $straatnamen[$data[0]] = $data[1];
    }
    fclose($handle);
}
//die;
$solved = array();

while($row = $result->fetch_assoc()){ 

	//print_r($row);
	$uri = "";
	$street = trim($row['txt_street'],',.‚ ');
	$orig_street = $street;
	echo $orig_street . "\n";

	if(isset($straatnamen[$street])){
		$uri = $straatnamen[$street];
		continue;
	}else{
		//echo $street . "\n";

		if(preg_match("/^([^\()]+)\(([^\)]+)\)/",$street,$found)){
			//print_r($found);
			$street = trim($found[2]) . " " . trim($found[1],',.‚ ');
		}



		foreach($straatnamen as $naam => $key){

			//$solved[$orig_street] = "uri";
			//continue;

			if(in_array($orig_street, $solved)){
				continue;
			}
			$from = array(" ");
			$needle = str_replace($from,"",$street);
			$hay = str_replace($from, "", $naam);

			if(levenshtein($needle,$hay) < 2){

				if(substr($needle,0,1) != substr($hay,0,1)){
					continue;
				}
				echo $street . "," . $naam . "," . $key . "\n";
				$solved[$orig_street] = $key;


			}
		}
		
		
		continue;
	}
}

print_r($solved);

$fp = fopen('more-ocr-variants.csv', 'w');
foreach ($solved as $key => $value) {
	$fields = array($key,$value);
	fputcsv($fp, $fields);
}
fclose($fp);


?>