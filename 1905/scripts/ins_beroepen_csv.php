<?

include("settings.php");

$run = false;

// read curated street list to array
$csv = '../data/beroepenlijst' . $jaar . '.csv';
$beroepen = array();
if (($handle = fopen($csv, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 10000, ",")) !== FALSE) {
        
        if(isset($beroepen[$data[1]])){
            echo "DAMN " . $data[1] . "\n";
        }else{
            $beroepen[$data[1]] = $data[0];
        }

    }
    fclose($handle);
}
//print_r(count($beroepen));
//die;

$csv = '../data/' . $jaar . '_whole_fixed.csv';

$i = 0;

if (($handle = fopen($csv, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
        
        $i++;

        if($i < 146591){     // startline prof section in file
            continue;
        }
        if($i == 174918){   // end of the prof section
            die;
        }
        
        

        $line = $data[0];

        if(isset($beroepen[$data[0]])){
            $current_profession = $beroepen[$data[0]];
        }

        $inssql = "insert into observations (scan,bbox,year,part,txt,txt_profession)
                    VALUES (
                        '" . $mysqli->real_escape_string(trim($data[2])) . "',
                        '" . $mysqli->real_escape_string(trim($data[1])) . "',
                        " . $jaar . ",
                        'profession',
                        '" . $mysqli->real_escape_string(trim($line)) . "',
                        '" . $mysqli->real_escape_string(trim($current_profession)) . "'
                    )";

        if($run){
            if (!$mysqli->query($inssql)) {
                printf("Error: %s\n", $mysqli->error);
                echo $inssql . ";\n";
            }
            echo ". ";
        }else{
            echo $inssql . ";\n";
        }

    }
    fclose($handle);
}




?>