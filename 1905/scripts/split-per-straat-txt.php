<?

include("settings.php");

$run = false;



$sql = "SELECT * FROM observations
        WHERE year = " . $jaar  . "
        AND txt_lastname IS NULL
        AND part = 'streets'
        ORDER BY id ASC";
$result = $mysqli->query($sql);


while($row = $result->fetch_assoc()){

    $txt = trim($row['txt'],',.‚| ');
    //echo $row['txt'] . "\n";
    //echo $txt . "\n";

    $entity_type = "";
    $prefix = "";
    $lastname = "";
    $number = "";
    $initials = "";
    $lastnameprefix = "";

    
    if(preg_match("/^([0-9]{1,3}) ([A-Z]\.( ?[A-Z]\.)?( ?[A-Z]\.)?) ([A-Z][a-zéëüè]+)$/", $txt,$found)){
        // 15 B. E. Blog
        //echo $txt . "\n";
        //print_r($found);
        $lastname = $found[5];
        $number = $found[1];
        $initials = $found[2];
        $lastnameprefix = "";
    }elseif(preg_match("/^([0-9]{1,3}) ([A-Z]\.( ?[A-Z]\.)?( ?[A-Z]\.)?) (van|de|van de|van der|van den|den|ten) ([A-Z][a-zéëüè]+)$/", $txt,$found)){
        // 116 P. van Drosthagen
        //echo $txt . "\n";
        //print_r($found);
        $lastname = $found[6];
        $number = $found[1];
        $initials = $found[2];
        $lastnameprefix = $found[5];
    }elseif(preg_match("/^([A-Z]\.( ?[A-Z]\.)?( ?[A-Z]\.)?) ([A-Z][a-zéëüè]+)$/", $txt,$found)){
        // H. H. J. Evers
        //echo $txt . "\n";
        //print_r($found);
        $lastname = $found[4];
        $number = "";
        $initials = $found[1];
        $lastnameprefix = "";
    }elseif(preg_match("/^([0-9]{1,3})? ?(Wed\.)( ?([A-Z][a-z]{0,4}\.( ?[A-Z][a-z]{0,4}\.)?( ?[A-Z][a-z]{0,4}\.)?))? (van|de|van de|van der|van den|den|ten)? ?([A-Z][a-zéëüè]+)$/", $txt,$found)){
        // 58 Wed. C.J. Warnsinck; 82 Wed. Const. van Goor; 6 Wed. van der Parrc
        //echo $txt . "\n";
        //print_r($found);
        $lastname = $found[8];
        $number = $found[1];
        $initials = $found[4];
        $prefix = $found[2];
        $lastnameprefix = $found[7];
    }elseif(preg_match("/^([A-Z]\.( ?[A-Z]\.)?( ?[A-Z]\.)?) (van|de|van de|van der|van den|den|ten) ([A-Z][a-zéëüè]+)$/", $txt,$found)){
        // C. H. de Jonge
        //echo $txt . "\n";
        //print_r($found);
        $lastname = $found[5];
        $number = "";
        $initials = $found[1];
        $lastnameprefix = $found[4];
    }elseif(preg_match("/^([0-9]{1,3})? ?(Gebr|Gebrs|Gezs)\.? ?(van|de|van de|van der|van den|den|ten)? ([A-Z][a-zéëüè]+)$/", $txt,$found)){
        // Gebr. Vredenburg
        //echo $txt . "\n";
        //print_r($found);
        $lastname = $found[4];
        $number = $found[1];
        $initials = "";
        $lastnameprefix = $found[3];
        $prefix = $found[2];
        $entity_type = "Gebroeders";
    }elseif(preg_match("/^([0-9]{1,3})? ?(Mej)\.?( ([A-Z][a-z]{0,4}\.( ?[A-Z][a-z]{0,4}\.)?( ?[A-Z][a-z]{0,4}\.)?))? ?(van|de|van de|van der|van den|den|ten)? ([A-Z][a-zéëüè]+)$/", $txt,$found)){
        // Mej. G. Hordijk; 20 Mej. C. van Vlaanderen
        //echo $txt . "\n";
        //print_r($found);
        $lastname = $found[8];
        $number = $found[1];
        $prefix = $found[2];
        $initials = $found[4];
        $lastnameprefix = $found[7];
    }elseif(preg_match("/^([A-Z][a-z]+\.( ?[A-Z][a-z]+\.)?( ?[A-Z][a-z]+\.)?) ([A-Z][a-zéëüè]+)$/", $txt,$found)){
        // Nico. Th. F. Stadtfeld
        //echo $txt . "\n";
        //print_r($found);
        $lastname = $found[4];
        $number = "";
        $initials = $found[1];
        $lastnameprefix = "";
    }elseif(preg_match("/^([0-9]{1,3}) ([A-Z][a-z]{0,4}\.( ?[A-Z][a-z]{0,4}\.)?( ?[A-Z][a-z]{0,4}\.)?) (van|de|van de|van der|van den|den|ten) ([A-Z][a-zéëüè]+)$/", $txt,$found)){
        // 16 Benj. A. Diamant
        //echo $txt . "\n";
        //print_r($found);
        $lastname = $found[6];
        $number = $found[1];
        $initials = $found[2];
        $lastnameprefix = $found[5];
    }else{
        echo $txt . "\n";
        echo "- ";
        continue;
    }
    

    //continue;

    $upd = "update observations set 
            txt_lastname = '" . $mysqli->real_escape_string($lastname) . "',
            txt_initials = '" . $mysqli->real_escape_string($initials) . "',
            txt_number = '" . $mysqli->real_escape_string($number) . "',
            txt_prefix = '" . $mysqli->real_escape_string($prefix) . "',
            txt_lastnameprefix = '" . $mysqli->real_escape_string($lastnameprefix) . "',
            entity_type = '" . $mysqli->real_escape_string($entity_type) . "'
            where id = " . $row['id'];

    if($run){
        if (!$mysqli->query($upd)) {
            printf("Error: %s\n", $mysqli->error);
            echo $upd . ";\n";
        }
        echo "+ ";
    }else{
        echo $upd . ";\n";
    }
    
}











?>