<?

include("../settings.php");

$run = true;

$street = "https://adamlink.nl/geo/street/jodenbreestraat/2158";
//$street = "https://adamlink.nl/geo/street/plantage-lijnbaansgracht/8174";
//$street = "https://adamlink.nl/geo/street/derde-oosterparkstraat/972";
$street = "https://adamlink.nl/geo/street/uilenburgerstraat/5667";

$sql = "SELECT DISTINCT(uri_street) FROM observations
		WHERE uri_street <> ''
		GROUP BY uri_street";
$result = $mysqli->query($sql);

while($row = $result->fetch_assoc()){ 
	echo "\n" . $row['uri_street'] . "\n";
	setnrs($row['uri_street']);
}


function setnrs($street){

	global $mysqli;
	global $run;

	$sql = "SELECT * FROM `observations` 
			WHERE uri_street = '" . $street . "'
			AND (txt_lastname is not null AND txt_lastname <> '')
			AND (txt_number is not null AND txt_number <> '')
			ORDER BY txt_lastname ASC";

	$result = $mysqli->query($sql);



	while($original = $result->fetch_assoc()){ 

		if(!$run){
			echo "\n\n";

			echo $original['year'] . " | ";
			echo $original['txt_lastname'] . " | ";
			echo $original['txt_initials'] . " | ";
			echo $original['txt_givenname'] . " | ";
			echo $original['txt_street'] . " | ";
			echo $original['txt_number'] . " | ";
			echo $original['txt_profession'] . " | ";
			echo $original['uri_street'] . " | \n";	
		}	


		$similar = array();
		
		// 1) Find same surname in this year
		$sql2 = "SELECT * FROM `observations` 
				WHERE uri_street = '" . $original['uri_street'] . "'
				AND txt_lastname LIKE '" . $mysqli->real_escape_string(substr($original['txt_lastname'],0,4)) . "%' 
				AND id <> " . $original['id'] . "";

		$result2 = $mysqli->query($sql2);

		
		$similar['same-nrs'] = array($original);
		$similar['diff-nrs'] = array();

		while($other = $result2->fetch_assoc()){ 

			$score = 0;

			if($original['txt_initials'] == $other['txt_initials'] && strlen($original['txt_initials'])){
				$score++;
			}

			if($original['txt_lastname'] == $other['txt_lastname'] && strlen($original['txt_lastname'])){
				$score++;
			}

			/*
			if(levenshtein($original['txt_profession'],$other['txt_profession']) < 3 && strlen($original['txt_profession'])){
				$score++;
			}
			*/

			if(levenshtein($original['txt_lastname'],$other['txt_lastname']) < 3 && strlen($original['txt_lastname'])){
				$score++;
			}

			if($score < 3){
				continue;
			}

			if($original['txt_number'] == $other['txt_number'] && strlen($other['txt_number'])){
				$similar['same-nrs'][] = $other;
			}elseif(strlen($other['txt_number'])){
				$similar['diff-nrs'][] = $other;
			}

		}

		
		// 2) Find same surname in next year
		$sql3 = "SELECT * FROM create_adresboeken_1907.`observations` 
				WHERE uri_street = '" . $original['uri_street'] . "'
				AND txt_lastname LIKE '" . $mysqli->real_escape_string(substr($original['txt_lastname'],0,4)) . "%'";

		$result3 = $mysqli->query($sql3);


		

		while($other = $result3->fetch_assoc()){ 

			$score = 0;

			//print_r($other);
			if($original['txt_initials'] == $other['txt_initials'] && strlen($original['txt_initials'])){
				$score++;
			}

			if($original['txt_lastname'] == $other['txt_lastname'] && strlen($original['txt_lastname'])){
				$score++;
			}

			/*
			if(levenshtein($original['txt_profession'],$other['txt_profession']) < 3 && strlen($original['txt_profession'])){
				$score++;
			}
			*/

			if(levenshtein($original['txt_lastname'],$other['txt_lastname']) < 3 && strlen($original['txt_lastname'])){
				$score++;
			}

			if($score < 3){
				continue;
			}

			if($original['txt_number'] == $other['txt_number'] && strlen($other['txt_number'])){
				$similar['same-nrs'][] = $other;
			}elseif(strlen($other['txt_number'])){
				$similar['diff-nrs'][] = $other;
			}

		}

		//print_r($similar);

		//echo "same: " . count($similar['same-nrs']) . " diff: " . count($similar['diff-nrs']);
		if(count($similar['same-nrs']) > count($similar['diff-nrs'])){
			$upd = "update observations set 
		            number = '" . $mysqli->real_escape_string($original['txt_number']) . "'
		            where id = " . $original['id'];

		    if($run){
		        if (!$mysqli->query($upd)) {
		            printf("Error: %s\n", $mysqli->error);
		            echo $upd . ";\n";
		        }
		        echo "+ ";
		    }else{
		        echo $upd . ";\n";
		    }
		}elseif(count($similar['diff-nrs']) > count($similar['same-nrs'])){
			$lastnr = false;
			$ok = true;
			foreach ($similar['diff-nrs'] as $dk => $dv) { // must not be different numbers
				if($lastnr && $dv['txt_number']!=$lastnr){
					$ok = false;
				}
				$lastnr = $dv['txt_number'];
			}
			if($ok){
				$upd = "update observations set 
			            number = '" . $mysqli->real_escape_string($lastnr) . "'
			            where id = " . $original['id'];

			    if($run){
			        if (!$mysqli->query($upd)) {
			            printf("Error: %s\n", $mysqli->error);
			            echo $upd . ";\n";
			        }
			        echo "change ";
			    }else{
			        echo $upd . ";\n";
			    }
			}
			

		}elseif(count($similar['diff-nrs']) == count($similar['same-nrs'])){
			//echo "SAME THING, DON'T DO ANYTHING ";
			$upd = "update observations set 
		            number = NULL
		            where id = " . $original['id'];

		    if($run){
		        if (!$mysqli->query($upd)) {
		            printf("Error: %s\n", $mysqli->error);
		            echo $upd . ";\n";
		        }
		        echo "- ";
		    }else{
		        echo $upd . ";\n";
		    }
		}
		if(!$run){
			echo "SAME: ";
			foreach ($similar['same-nrs'] as $dk => $dv) {
				echo $dv['txt_number'] . " ";
			}
			echo " DIFF: ";
			foreach ($similar['diff-nrs'] as $dk => $dv) {
				echo $dv['txt_number'] . " ";
			}
		}


	}
}


?>