<?

include("settings.php");

$run = false;

$csv = '../data/' . $jaar . '_whole_fixed.csv';

$i = 0;

if (($handle = fopen($csv, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
        
        $i++;

        if($i < 14611){     // startline alphabetical section in file
            continue;
        }
        if($i == 81083){   // end of the alphabetical section
            die;
        }

        $line = $data[0];

        $isobservation = "yes";

        if(strlen(trim($line)) < 5){
            //continue;
            $isobservation = "no";
        }
        if(!strlen(trim($line))){
            continue;
        }
        if(!preg_match("/\(/", $line) && !preg_match("/\)/", $line)){
            //echo "" . $line;
            //continue;
            $isobservation = "no";
        }
        if(!preg_match("/[0-9]/", $line)){
            //continue;
            $isobservation = "no";
        }else{
            //echo "+++ " . $line;
        }
        
        if($isobservation == "no"){
            //echo "--- " . $line;
        }else{
            //echo "+++ " . $line;
        }
        

        //continue;
        $inssql = "insert into observations (scan,bbox,year,part,txt,is_observation)
                    VALUES (
                        '" . $mysqli->real_escape_string(trim($data[2])) . "',
                        '" . $mysqli->real_escape_string(trim($data[1])) . "',
                        " . $jaar . ",
                        'alphabetical',
                        '" . $mysqli->real_escape_string(trim($line)) . "',
                        '" . $isobservation . "'
                    )";

        if($run){
            if (!$mysqli->query($inssql)) {
                printf("Error: %s\n", $mysqli->error);
                echo $inssql . ";\n";
            }
            echo ". ";
        }else{
            echo $inssql . ";\n";
        }


    }
    fclose($handle);
}




?>